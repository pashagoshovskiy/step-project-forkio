document.addEventListener("DOMContentLoaded", evt => {
    const menuBurger = document.querySelector('.home__header-nav-burger');
    const navMenu = document.querySelector('.home__header-nav-menu');
    menuBurger.addEventListener('click', () => {

        menuBurger.classList.toggle('home__header-nav-burger--active');


        if (menuBurger.classList.contains('home__header-nav-burger--active')) {
            navMenu.classList.add('home__header-nav-menu--active');
        } else {
            navMenu.classList.remove('home__header-nav-menu--active');
        }

    })
})