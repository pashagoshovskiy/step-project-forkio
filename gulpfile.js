const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean')
const browserSync = require('browser-sync').create()
const concat = require('gulp-concat')
const imagemin = require('gulp-imagemin');
const minify = require('gulp-minify');


const path = {
    src:{
        img:'./src/img/**/*',
        js: './src/js/**/*',
        scss: './src/scss/**/*.scss',
        html: './index.html'
    },
    dist:{
        img:'./dist/img',
        js:'./dist/js',
        css:'./dist/css',
        root: '.dist'
    }
}

//function

const createStyle = ()=>{
    return gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.dist.css))

}

const createImg = () =>{
    return gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.dist.img))
}

const createJS = () =>{
    return gulp.src(path.src.js)
        .pipe(minify())
        .pipe(gulp.dest(path.dist.js))
}

const cleanDist = () =>{
    return gulp.src(path.dist.root, {allowEmpty: true})
        .pipe(clean())
}

// watcher
const watcher = ()=>{
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(path.src.scss,createStyle).on('change', browserSync.reload)
    gulp.watch(path.src.img,createImg).on('change', browserSync.reload)
    gulp.watch(path.src.js,createJS).on('change', browserSync.reload)
    gulp.watch(path.src.html).on('change', browserSync.reload)
}

//task
gulp.task('build', gulp.series(cleanDist, createImg, createJS, createStyle))
gulp.task('dev', watcher)